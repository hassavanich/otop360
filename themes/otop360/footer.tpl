{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($content_only) || !$content_only}
					</div><!-- #center_column -->
					{if isset($right_column_size) && !empty($right_column_size)}
						<!-- <div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{*$HOOK_RIGHT_COLUMN*}</div> -->
					{/if}
					</div><!-- .row -->
				</div><!-- #columns -->
			</div><!-- .columns-container -->

			{if $page_name == 'product'}
				{if isset($attachments) && $attachments}
					<article id="vr_page" class="vr-page out">
						<header class="vr-header-container">
							<div class="vr-logo-container">
								<a href="{$base_dir}" title="{$shop_name|escape:'html':'UTF-8'}">
									<img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/>
								</a>
							</div>
							<div class="vr-headline">
								<h1 class="vr-headline-name">3D Viewer</h1>
								<h1 class="vr-headline-product-name" itemprop="name">
									{$product->name|escape:'html':'UTF-8'}
								</h1>
							</div>
							<div class="vr-close-button-container">
								<button id="vr_close" class="vr-close-button">Back to Product Detail</button>
							</div>
						</header>
						<div class="vr-container">
							{foreach from=$attachments item=attachment}
						        {assign var=filePath value=$download_dir|cat:$attachment.file}
						        {assign var=fileSize value=($filePath|@filesize)/1024}
						        {assign var=filePathName value="`$base_dir`download/`$attachment.file`"}
							        <!-- <a href="{$link->getPageLink('attachment', true, NULL, "id_attachment={$attachment.id_attachment}")}">{$filePathName|escape:'htmlall':'UTF-8'}</a> -->

							        <!-- 3D Scene with X3DOM-->
							        <x3d id="vr_viewer" class="vr-viewer">
								        <param name="showLog" value="false"></param>
								        <param name="showStat" value="false"></param>
								        <param name="showProgress" value="true"></param>
								        <param name="disableDoubleClick" value="false"></param>
								        <param name="disableRightDrag" value="true"></param>
								        <scene>
							            	<transform translation="0 0 0">
									            <inline url="{$filePathName|escape:'htmlall':'UTF-8'}" />
									        </transform>
							            </scene>
						            </x3d>

						            <!-- Camera background with WebRTC -->
						            <video id="video_camera" class="video-camera out"></video>

						            <!-- Navigation Control -->
						            <section class="navigation-control">
							            <ul class="control-list">
								            <li class="control-item">
									            <button id="move" class="control-button">
										            Move
									            </button>
								            </li>
								            <li class="control-item">
									            <button id="rotate_zoom" class="control-button">
										            Rotate/Zoom
									            </button>
								            </li>
							            </ul>
						            </section>

						            <!-- Background Uploader -->
						            <div class="vr-toolbar-container">
						            	<div id="camera_toggle_container" class="camera-toggle-button-container">
							            	<button id="camera_toggle" class="camera-toggle-button">
								            	Open Camera
							            	</button>
						            	</div>
										<div class="input-group">
											<span class="input-group-addon">Background Image</span>
								            <input class="form-control" type="file" id="files" name="files[]" />
											<output id="list"></output>
										</div>
									</div>
						    {/foreach}
						</div>
					</article>
				{/if}
			{/if}

			{if isset($HOOK_FOOTER)}
				<!-- Footer -->
				<div class="footer-container">
					<footer id="footer"  class="container">
						<div class="row">
							<div class="copyright-container">
								Copyright &copy; {$smarty.now|date_format:"%Y"} OTOP360&trade;, All rights reserved.
							</div>
							{$HOOK_FOOTER}
						</div>
					</footer>
				</div><!-- #footer -->
			{/if}
		</div><!-- #page -->
{/if}
{include file="$tpl_dir./global.tpl"}
	{*debug*}

	{if $page_name == 'product'}
		<script type="text/javascript" src="{$js_dir}x3dom.js"></script>
		<script type="text/javascript" src="{$js_dir}gumwrapper.min.js"></script>
		<script type="text/javascript" src="{$js_dir}gui.js"></script>
		<script type="text/javascript" src="{$js_dir}vr.js"></script>
	{/if}
	</body>
</html>