$(document).ready(function() {
    $('#vr_open, #vr_close').click(function() {
        if ($('#vr_page').hasClass('out')) {
            $('#vr_page').animate({left: '0px'}, 300).removeClass('out');
            $('html, body').animate({scrollTop: 0}, 300);
            $('html, body').addClass('fit');
        } else {
            $('#vr_page').animate({left: '100%'}, 300).addClass('out');
            $('html, body').removeClass('fit');
        }
    })
});