// X3DOM Runtime
x3dom.runtime.ready = function() {

	var vr = document.getElementById('vr_viewer');

	setTimeout(function() {
		vr.runtime.resetView();
		vr.runtime.showAll();
	}, 3000);

	$('#move').click(function() {
		vr.runtime.lookAround();
	})

	$('#rotate_zoom').click(function() {
		vr.runtime.examine();
	})
};

// Background File Upload
function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

		// Only process image files.
		if (!f.type.match('image.*')) {
	        continue;
		}

		var reader = new FileReader();

		// Closure to capture the file information.
		reader.onload = (function(theFile) {
			return function(e) {
				// Render thumbnail.
				// var span = document.createElement('span');
				// span.innerHTML = ['<img class="thumb" src="', e.target.result,
				// '" title="', escape(theFile.name), '"/>'].join('');
				// document.getElementById('list').insertBefore(span, null);

				// var style = document.createElement('style');
				// style.type = 'text/css';
				// style.innerHTML = '#3d_viewer { background: url("' + e.target.result + '") no-repeat center center fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;}';
				// document.getElementsByTagName('x3d')[0].innerHTML(style);

				document.getElementById("vr_viewer").style.background = 'url("' + e.target.result + '") no-repeat center center';
				document.getElementById("vr_viewer").style.backgroundSize = "cover";

				// reader.onload = function (e) {
                // $('.3d-viewer').css('background', 'transparent url('+e.target.result +') left top no-repeat');
	            // }
			};
		})(f);

		// Read in the image file as a data URL.
		reader.readAsDataURL(f);
    }
}
document.getElementById('files').addEventListener('change', handleFileSelect, false);

// WebRTC
$(document).ready(function() {
	(function() {
	    function showSuccess(video) {
	        // alert('Hey, it works! Dimensions: ' + video.videoWidth + ' x ' + video.videoHeight);
	    }
	    function showError(error) {
	        // alert('Oops: ' + error.message);
	        $('#camera_toggle_container').css('display', 'hidden');
	    }
	    var gum = new GumWrapper({video: 'video_camera'}, showSuccess, showError);
	    gum.play();
	})();

	$('#camera_toggle').click(function() {
		if ($('#video_camera').hasClass('out')) {
			$('.vr-viewer').css('background', 'transparent');
			$('#video_camera').css('display', 'block');
			$('#video_camera').removeClass('out');
		} else {
			$('.vr-viewer').css('background', '#ffffff');
			$('#video_camera').css('display', 'none');
			$('#video_camera').addClass('out');
		}
	})
});